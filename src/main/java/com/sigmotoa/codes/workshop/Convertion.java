package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static int kmToM1(double km)
    {
        return  (int)(km * 1000);
    }

//Km to metters
    public static double kmTom(double km)
    {
        return (int)(km * 1000);
    }
    
    //Km to cm
    public static double kmTocm(double km)
    {
        return (int)(km * 100000);
    }

//milimetters to metters
    public static double mmTom(int mm)
    {
        return (mm * 0.001);
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        return (int)(miles * 5280);
    }

//convert yards to inches
    public static int yardToInch(int yard)
    {
        return yard * 36;
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        return inch / 63360;
    }
//convert foot to yards
    public static int footToYard(int foot)
    {
        return foot / 3;
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {return Double.parseDouble(km) * 39370.079;}

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {return Double.parseDouble(mm) / 304.8;}
//convert yards to cm    
    public static double yardToCm(String yard)
    {return Double.parseDouble(yard) * 91.44;}


}
