package com.sigmotoa.codes.workshop;

import org.w3c.dom.ls.LSOutput;

import javax.swing.*;
import java.util.Scanner;

/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */
public class Agility {

    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB){
        double num1 = Double.parseDouble(numA);
        double num2 = Double.parseDouble(numB);

        if (num1 > num2){
            return  true;
        }
        else {
            return false;
        }
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE) {
        int[] array = {numA, numB, numC, numD, numE};
        int aux;

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    aux = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = aux;
                }
            }
        }
        return array;
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array []){
        int menor = 0;

        for (int i = 1; i < array.length;i++){
            if (array[i] < array[menor]){
                menor = i;
            }
        }

        return array[menor];
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA){
            int comp = 0, ulti, aux;
            aux = numA;

            while (aux != 0){
                ulti = aux % 10;
                comp = comp * 10 + ulti;
                aux = aux / 10;
            }
            return numA == comp;
    }

    //the word is palindrome
    public static boolean palindromeWord(String word) {
        int inicio = 0, fin = word.length() - 1;

        while (inicio < fin) {
            if (word.charAt(inicio) != word.charAt(fin)) {
                return false;
            } else {
                inicio++;
                fin--;
            }
        }
        return true;
    }

    //Show the factorial number for the parameter
   public static int factorial(int numA){

        return 0;
    }

   //is the number odd
   public static boolean isOdd(byte numA)
   { return false; }

   //is the number prime
   public static boolean isPrimeNumber(int numA)
   { return false; }

   //is the number even
    public static boolean isEven(byte numA)
    { return false; }

    //is the number perfect
    public static boolean isPerfectNumber(int numA)
    { return false; }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    { return new int[0];}

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA)
    {return -1;}

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */


    { return null;}
}
